#include<bits/stdc++.h>

#define FITNESS_LIMIT           162*162
#define GA_POPSIZE		50		// ga population size
#define GA_MAXITER		10		// maximum iterations
#define GA_ELITRATE		0.40f		// elitism rate
#define GA_MUTATIONRATE         0.95f		// mutation rate
#define GA_MUTATION		RAND_MAX * GA_MUTATIONRATE
#define TSIZE                   4

using namespace std; // polluting global namespace, but hey...

string defaultMove[]={"RIGHT", "LEFT", "DOWN", "UP"};
bool playerVertical[] = {true, true, false, false};
enum direction {Right, Left, Down, Up};

class Wall {
public:
    int x, y;
    bool vertical;

    Wall() {
        x = -1;
        y = -1;
        vertical = false;
    }

    Wall(int a, int b, bool v) {
        x = a;
        y = b;
        vertical = v;
    }
};

class Player{
    public:
        int x, y, wallsLeft, id;
};

class Board{
    public:
        int w, h, playerCount, myId, wallCount;
        Player* players;
        Wall* walls;
        bool Walls[9][9][2];
        
        int getTarget(){
            return getTarget(myId);
        }
        
        int getTarget(int id){
            switch(id){
                case 0:
                    return (w-1) + 10*players[id].y;
                case 1:
                    return 10*players[id].y;
                case 2:
                    return players[id].x + 10*(h-1);
                case 3:
                    return players[id].x;
            }
        }
        
        int getTarget(int id, int index){
            switch(id){
                case 0:
                    return (w-1) + 10*index;
                case 1:
                    return 10*index;
                case 2:
                    return index + 10*(h-1);
                case 3:
                    return index;
            }
        }
        
        string getMove(Player player, int v){
            int x = v%10;
            int y = v/10;
            
            cerr<<player.x <<"->"<< x<<","<<player.y <<"->"<< y<<endl;
            if(player.x < x)
                return "RIGHT";
                
            if(player.x > x)
                return "LEFT";
                
            if(player.y < y)
                return "DOWN";
                
            if(player.y > y)
                return "UP";
            
            return defaultMove[myId];
        }
        
        void toggleWall(Wall wall, bool put){
            Walls[wall.x][wall.y][wall.vertical?1:0] = put;
        }
        
        bool checkWall(Wall wall) {
            int aX = wall.x, aY = wall.y;
            bool build = true;
            if (wall.vertical) {
                build = aY >= 0 && aY < h - 1;
                build = build && aX > 0 && aX < w;
                build = build && !Walls[aX][aY][1]; //v wall itself
//                cerr << "build: " << build << endl;
                if (aY > 0)
                    build = build && !Walls[aX][aY - 1][1]; //v wall 1 step up
                if (aY < h - 2)
                    build = build && !Walls[aX][aY + 1][1]; //v wall 1 step down
                build = build && !Walls[aX - 1][aY + 1][0]; //h wall 1 step left
            } else {
                build = aX >= 0 && aX < w - 1;
                build = build && aY > 0 && aY < h;
                build = build && !Walls[aX][aY][0]; //h wall itself
                if (aX > 0)
                    build = build && !Walls[aX - 1][aY][0]; //h wall 1 step left
                if (aX < w - 2)
                    build = build && !Walls[aX + 1][aY][0]; //h wall 1 step right
                build = build && !Walls[aX + 1][aY - 1][1]; //v wall 1 step up
            }        
            if (build) {
                return true;
            }
            return false;
        }

        bool checkWall(Wall wall, int enemy) {
        int aX, aY;
        bool build;
        if (wall.vertical) {
            if (enemy) {
                aX = wall.x + 1;
            } else {
                aX = wall.x;
            }
            for (aY = wall.y; aY >= wall.y - 1; aY--) {
                build = true;
                build = aY >= 0 && aY < h - 1;
                build = build && aX > 0 && aX < w;
                build = build && !Walls[aX][aY][1]; //v wall itself
                cerr << "build: " << build << endl;
                if (aY > 0)
                    build = build && !Walls[aX][aY - 1][1]; //v wall 1 step up
                if (aY < h - 2)
                    build = build && !Walls[aX][aY + 1][1]; //v wall 1 step down
                //                if(aX < w -1)
                //                    build = build && !Walls[aX][aY+1][0];//h wall at same x
                build = build && !Walls[aX - 1][aY + 1][0]; //h wall 1 step left
                if (build) {
                    cout << aX << " " << aY << " V" << endl;
                    return true;
                }
            }
        } else {
            aY = wall.y;
            for (aX = wall.x; aX >= wall.x - 1; aX--) {
                build = true;
                build = aX >= 0 && aX < w - 1;
                build = build && aY > 0 && aY < h;
                build = build && !Walls[aX][aY][0]; //h wall itself
                if (aX > 0)
                    build = build && !Walls[aX - 1][aY][0]; //h wall 1 step left
                if (aX < w - 2)
                    build = build && !Walls[aX + 1][aY][0]; //h wall 1 step right
                //                if(aY < h - 1)
                //                    build = build && !Walls[aX+1][aY][1];//v wall at same y
                build = build && !Walls[aX + 1][aY - 1][1]; //v wall 1 step up
                if (build) {
                    cout << aX << " " << aY << " H" << endl;
                    return true;
                }
            }
        }
        return false;
    }
    
}board;

class Graph{
    
    int dist[100];
    int previous[100];
    int costs[100][100];
    int links[100][4];
    int linkCount;
        
    public:
    void createGraph(int w, int h){
        memset(links, -1, sizeof(links));
        linkCount = 89;
        
        for(int x = 0; x < w; x++){
            for(int y = 0; y < h; y++){
                int id = x + 10 * y;
                if(x < w)
                    links[id][Right] = id+1;
                if(x > 0)
                    links[id][Left] = id-1;
                if(y < h)
                    links[id][Down] = id+10;
                if(y > 0)
                    links[id][Up] = id-10;
            }
        }
    }
    
    void calcThreats(){
        for(int u = 0; u < linkCount; u++){
            for(int j = 0; j < 4; j++){
                int v = links[u][j];
                if(v < 0)
                    continue;
                costs[u][v] = costs[v][u] = 1; 
            }
        }
    }
    
    void Dijkstra(int source){
     	// Distance from source to source
    	dist[source] = 0;
    	std::set<std::pair<int,int> > Q;
    	previous[source] = -1;
    			
    	Q.insert(make_pair(dist[source], source));
    	// Initializations
     	for(int v = 0; v < linkCount; v++){
            if(v != source){
                // Unknown distance function from source to v
                dist[v] = INT_MAX;
                // Previous node in optimal path from source
                previous[v] = -1;
            }
    	}
//        cerr<<"u,v: "<<endl;
    	
    	// The main loop
    	while (!Q.empty()) {
            // Source node in first case
            //vertex in Q with min dist[u];
            int u = Q.begin()->second;
            Q.erase(Q.begin());

            // where v has not yet been removed from Q.
            for (int i = 0; i < 4; i++) {
                int v = links[u][i];
                if(v < 0)
                    continue;
//                cerr<<u<<","<<v<<" ";
                int cost = dist[u] + costs[u][v];
                // A shorter path to v has been found
                if (cost < dist[v]) {
                    Q.erase(make_pair(dist[v], v));
                    dist[v] = cost;
                    previous[v] = u;
                    Q.insert(make_pair(dist[v], v));
                }
            }
        }
//        cerr<<endl;
    }
    
    std::list<int> getShortestPathTo(int zId)
    {
        std::list<int> path;
        
        for (int v = zId; previous[v] != -1; v = previous[v]){
            path.push_front(v);
            //cerr<<" "<<v<<",";
        }
        //cerr<<endl;
        return path;
    }
    
    std::list<int> getShortestPathsTo(int id)
    {
        list<int> minPath;
        int limit = 9, min = 81;//, target;
        limit = id / 2 ? board.w: board.h;
        for(int i = 0; i < limit; i++){
            list<int> path = getShortestPathTo(board.getTarget(id, i));
            if(path.size() && path.size() < min){
                min = path.size();
                //target = board.getTarget(id, i);
                minPath = path;
            }
        }
        return minPath;
    }
    
    void erase(int id1, int id2){
        for (int i = 0; i < 4; i++){
            if(links[id1][i] == id2){
                links[id1][i] = -1;
            }
            
            if(links[id2][i] == id1){
                links[id2][i] = -1;
            }
        }
    }
    
    void putWall(Wall w){
        if(w.vertical){
            int id = (w.x-1) + 10*w.y;
            erase(id, id+1);
            erase(id+10, id+11);
        }else{
            int id = w.x + 10*(w.y-1);
            erase(id, id+10);
            erase(id+1, id+11);
        }
    }
}graph;

class Bfs{
    
    struct Wall_fitness{
        Wall wall;
        float fitness;
        
        void update(Wall w, float f){
            wall.x = w.x;
            wall.y = w.y;
            wall.vertical = w.vertical;
            fitness = f;
        }
        
        bool operator() (Wall_fitness x, Wall_fitness y) const {
                return (x.fitness < y.fitness);
        }
        
    };
    
    
    float calc_fitness(Wall w, Graph g, Board b) {
        int pathSize[b.playerCount - 1], id, count = 0, myPathSize;
        list<int> path;
        g.putWall(w);
        
        for(int i = 0; i < b.playerCount; i++){
            id = b.players[i].x + b.players[i].y * 10;
            if(id < 0){
                pathSize[i] = -1;
                continue;
            }
            g.Dijkstra(id);
            path = g.getShortestPathsTo(i);
            if(path.size() == 0){
                //cerr<<"path = 0"<<endl;
                return 81;
            }
            //cerr<<"p:"<<i<<","<<path.size()<<" ";
            if(i == board.myId)
                myPathSize = path.size();
            else{
                pathSize[count++] = path.size();
            }
        }
        //cerr <<"mps "<< myPathSize<<endl;
        float a, score = 1;
        for(int i = 0; i < count; i++)
        {
            a = myPathSize*1.0/pathSize[i];
            score *= a;
            //cerr<<"s "<<score<<" a "<<a<<endl;
        }
        
//        if(score < 1)
//            cerr<<"s "<<score<<" mps "<<myPathSize<<" p0 "<<pathSize[0]<<" p1 "<<pathSize[1]<<endl;
        
        return score;
    }
    
    void calc_fitness(std::vector<Wall_fitness> &population, Graph g, Board b) {

        float fitness;
        //cerr<<"me "<<board.myId<<endl;
        for (int i = 0; i < population.size(); i++) {
            Wall wall = population[i].wall;
            if(b.checkWall(wall)){
                fitness = calc_fitness(wall, g, b);
            }
            else{
                fitness = 81;
            
            }
            if(fitness < 1)
                cerr<<wall.x<<" "<<wall.y<<" "<<wall.vertical<<" "<<fitness<<endl;
            
            population[i].fitness = fitness;
        }
    }

    void sort_by_fitness(std::vector<Wall_fitness> &population) {
        std::sort(population.begin(), population.end(), Wall_fitness());
    }
    
    public:

    Wall main(float score, Graph g, Board b) {

        std::vector<Wall_fitness> pop_alpha;
        std::vector<Wall_fitness> *population;

        
        //init_population(pop_alpha);
        for(int x = 0; x < b.w; x++){
            for(int y = 0; y < b.h; y++){
                Wall_fitness tmp;
                Wall tmpWallV(x, y, true);
                Wall tmpWallH(x, y, false);
                if(b.checkWall(tmpWallV)){
                    tmp.update(tmpWallV,0);
                    pop_alpha.push_back(tmp);
                }   
                if(b.checkWall(tmpWallH)){
                    tmp.update(tmpWallH,0);
                    pop_alpha.push_back(tmp);
                }   
            }
        }
        population = &pop_alpha;

        //best = (*population)[0];
        //memcpy(pop_alpha[0].pos, best.pos, sizeof(pop_alpha[0].pos));
//        Wall_fitness best;
//        best.fitness = 81;
        Wall wall;
        
        //for (int i = 0; i < GA_MAXITER; i++) {
            calc_fitness(*population, g, b); // calculate fitness
            sort_by_fitness(*population); // sort them
            //print_best(*population); // print the best one

            //print_best(*population);
            //cout << i;
            if ((*population)[0].fitness < score) {
                for(int i = 0; i < 5; i++){
                    wall = (*population)[i].wall;
                    cerr<<"best ("<<wall.x<<", "<<wall.y<<", "<<wall.vertical<<") "<<(*population)[i].fitness<<endl;
                }
                wall = (*population)[0].wall;
            }
        //}

        return wall;
    }
} bfs;

class MMX{
    
    float max_fitness, min_fitness;
    struct Wall_fitness{
        Wall wall;
        float fitness;
        std::vector<Wall_fitness> *pop;
        
        void update(Wall w, float f){
            wall.x = w.x;
            wall.y = w.y;
            wall.vertical = w.vertical;
            fitness = f;
        }
        
        bool operator() (Wall_fitness x, Wall_fitness y) const {
                return (x.fitness < y.fitness);
        }
    };
    
    std::vector<Wall_fitness> population;
    
    float calc_fitness(Wall w, Graph g, Board b) {
        int pathSize[b.playerCount - 1], id, count = 0, myPathSize;
        list<int> path;
        g.putWall(w);
        
        for(int i = 0; i < b.playerCount; i++){
            id = b.players[i].x + b.players[i].y * 10;
            if(id < 0){
                pathSize[i] = -1;
                continue;
            }
            g.Dijkstra(id);
            path = g.getShortestPathsTo(i);
            if(path.size() == 0){
                //cerr<<"path = 0"<<endl;
                return 81;
            }
            //cerr<<"p:"<<i<<","<<path.size()<<" ";
            if(i == b.myId)
                myPathSize = path.size();
            else{
                pathSize[count++] = path.size();
            }
        }
        //cerr <<"mps "<< myPathSize<<endl;
        float a, score = 1;
        for(int i = 0; i < count; i++)
        {
            a = myPathSize*1.0/pathSize[i];
            score *= a;
            //cerr<<"s "<<score<<" a "<<a<<endl;
        }
        
//        if(score < 1)
//            cerr<<"s "<<score<<" mps "<<myPathSize<<" p0 "<<pathSize[0]<<" p1 "<<pathSize[1]<<endl;
        
        return score;
    }
    
    void calc_fitness(std::vector<Wall_fitness> &population, Graph g, Board b) {

        float fitness;
        max_fitness = 0;
        min_fitness = 81;
        //cerr<<"me "<<board.myId<<endl;
        for (int i = population.size()-1; i >= 0 ; i--){
            Wall wall = population[i].wall;
            if(b.checkWall(wall)){
                fitness = calc_fitness(wall, g, b);
            }
            else{
                fitness = 81;
            }
//            if(fitness < 1)
//                cerr<<wall.x<<" "<<wall.y<<" "<<wall.vertical<<" "<<fitness<<endl;
            if(fitness > 80)
                population.erase(population.begin()+i);
            else{
                population[i].fitness = fitness;
                if(fitness > max_fitness)
                    max_fitness = fitness;
                if(fitness < min_fitness)
                    min_fitness = fitness;
            }
        }
        
        //for(std::vector<Wall_fitness>::iterator t = population.begin(); t != population.end(); t++){
    //}
    }

    void sort_by_fitness(std::vector<Wall_fitness> &population) {
        std::sort(population.begin(), population.end(), Wall_fitness());
    }
 
        
    void init_population(Board b){
        population.clear();
        for(int p = 0; p < b.playerCount; p++){
            if(p == b.myId)
                continue;
            for(int x = max(0, b.players[p].x-4); x < min(b.w, b.players[p].x+4); x++){
                for(int y = max(0, b.players[p].y-4); y < min(b.h, b.players[p].y+4); y++){
                    Wall_fitness tmp;
                    Wall tmpWallV(x, y, true);
                    Wall tmpWallH(x, y, false);
                    if(!b.Walls[x][y][true]){
                        tmp.update(tmpWallV,0);
                        b.Walls[x][y][true] = true;
                        population.push_back(tmp);
                    }   
                    if(!b.Walls[x][y][false]){
                        tmp.update(tmpWallH,0);
                        b.Walls[x][y][false] = true;
                        population.push_back(tmp);
                    }
                }
            }
        }
    }
    
    void init_population(std::vector<Wall_fitness> source, std::vector<Wall_fitness> &pop){
        pop = source;
    }
    
    float minimax(Wall_fitness &wf, int depth, Graph g, Board b) {
        float bestValue, val;
        b.myId = (b.myId + 1) % b.playerCount;
        b.toggleWall(wf.wall, true);
        g.putWall(wf.wall);
        
        std::vector<Wall_fitness> pop;
        if(depth > 0){
            init_population(population, pop);
        }
        wf.pop = &pop;
        
        if (depth == 1){
            calc_fitness(pop, g, b);
            if (b.myId != board.myId){
                return max_fitness;
            }
            else{
                return min_fitness;
            }
        }
//        
//        if (depth == 0){
//            return calc_fitness(wf.wall, g, b);
//        }
        
        if (b.myId != board.myId){
            bestValue = 0;
            for(int i = 0; i < wf.pop->size(); i++){
                Wall_fitness wallf = (*wf.pop)[i];
                val = minimax(wallf, depth - 1, g, b);
                bestValue = max(bestValue, val);
            }
        }
        else{
            bestValue = 81;
            for(int i = 0; i < wf.pop->size(); i++){
                Wall_fitness wallf = (*wf.pop)[i];
                val = minimax(wallf, depth - 1, g, b);
                bestValue = min(bestValue, val);
            }
        }
        return bestValue;
    }
    
    public:

    Wall main(float score, Graph g, Board b) {

        std::vector<Wall_fitness> pop_alpha;
        Wall wall;
        
        init_population(b);
        init_population(population, pop_alpha);
        
//        for(int i = 0; i < 32; i++){
//            calc_fitness(*population, g, b);
//            sort_by_fitness(*population);
//            cerr<<i<<" pop size: "<<population->size()<<endl;
//        }
        
        calc_fitness(pop_alpha, g, b); // calculate fitness
        cerr<<"pop size: "<<pop_alpha.size()<<endl;
        sort_by_fitness(pop_alpha); // sort them
        
        std::vector<Wall_fitness> top_pop;
        if(pop_alpha.size()){
            for(int i = 0; i < pop_alpha.size() && i < 5 && pop_alpha[i].fitness <= score && pop_alpha[i].fitness <= pop_alpha[0].fitness; i++){
                Wall_fitness wallf = pop_alpha[i];
                wallf.fitness = minimax(wallf, 1, g, b);
                top_pop.push_back(wallf);
                cerr<<"best ("<<wallf.wall.x<<", "<<wallf.wall.y<<", "<<wallf.wall.vertical<<") "<<wallf.fitness<<endl;
            }

            sort_by_fitness(top_pop);
            wall = top_pop.back().wall;
        }

        return wall;
    }
} mmx;

int getPathSizeAfterWall(Wall w, Player p, Graph g){
    g.putWall(w);
    g.Dijkstra(p.y*10+p.x);
    return g.getShortestPathsTo(p.id).size();
}

std::set<pair<int,int> > checkPathForMoreWalls(list<int> path, Player p){
    int i = 0, count = 0, max = 0;
    std::set<pair<int,int> > walls;
    for(std::list<int>::iterator it = path.begin(); it != path.end(); it++){
        i++;
        int pos = (*it);
        int x = pos % 10, y = pos / 10;
        Wall wV(x, y, true), wV1(x, y-1, true), wH(x, y, false), wH1(x-1, y, false);
        int size = 0;
        if(board.checkWall(wV)){
            size = getPathSizeAfterWall(wV, p, graph);
            if(size != 0){
                if(size > path.size() ){
                    count++;
                    walls.insert(make_pair(wV.x*10+wV.y*100+wV.vertical, size));
                    cerr<<x<<y<<"V"<<size<<endl;
                }
            }
        }
        if(board.checkWall(wV1)){
            size = getPathSizeAfterWall(wV1, p, graph);
            if(size != 0){
                if(size > path.size() ){
                    count++;
                    walls.insert(make_pair(wV1.x*10+wV1.y*100+wV1.vertical, size));
                    cerr<<x<<y-1<<"V"<<size<<endl;
                }
            }
        }
        if(board.checkWall(wH)){
            size = getPathSizeAfterWall(wH, p, graph);
            if(size != 0){
                if(size > path.size() ){
                    count++;
                    walls.insert(make_pair(wH.x*10+wH.y*100+wH.vertical, size));
                    //cerr<<x<<y<<"H"<<size<<endl;
                }
            }
        }
        if(board.checkWall(wH1)){
            size = getPathSizeAfterWall(wH1, p, graph);
            if(size != 0){
                if(size > path.size() ){
                    count++;
                    walls.insert(make_pair(wH1.x*10+wH1.y*100+wH1.vertical, size));
                    //cerr<<x-1<<y<<"H"<<size<<endl;
                }
            }
        }
    }
    return walls;
}

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
int main(){
    cin >> board.w >> board.h >> board.playerCount >> board.myId; cin.ignore();
    
    Player players[board.playerCount];
    board.players = players;
    graph.createGraph(board.w, board.h);
    
    bool deadPlayer = false;
    
    while (1) {
        for (int i = 0; i < board.playerCount; i++) {
            cin >> players[i].x >> players[i].y >> players[i].wallsLeft; cin.ignore();
            players[i].id= i;
            if(players[i].x < 0 or players[i].y < 0)
                deadPlayer = true;
        }
        
        cin >> board.wallCount; cin.ignore();
        Wall walls[board.wallCount];
        board.walls = walls;
        memset(board.Walls, false, sizeof(board.Walls));
        
        for (int i = 0; i < board.wallCount; i++) {
            string wallOrientation; // wall orientation ('H' or 'V')
            cin >> walls[i].x >> walls[i].y >> wallOrientation; cin.ignore();
            int b = walls[i].vertical = (wallOrientation == "V")? 1 : 0;
            if(!board.Walls[walls[i].x][walls[i].y][b]){
                board.toggleWall(walls[i], true);
                graph.putWall(walls[i]);
            }
        }        
        graph.calcThreats();
        
        int enemy;
        list<int> enemyPath, myPath;
        int pathSize[board.playerCount - 1], count = 0, myPathSize;
        int oneMoreWall[board.playerCount - 1];
        
        for(int i = 0; i < board.playerCount; i++){
            list<int> path;
            int id;
            id = board.players[i].x + board.players[i].y * 10;
            if(id < 0){
                pathSize[i] = -1;
                continue;
            }
            graph.Dijkstra(id);
            path = graph.getShortestPathsTo(i);
            if(path.size() == 0){
                //cerr<<"path = 0"<<endl;
                continue;
            }
            if(i == board.myId){
                myPathSize = path.size();
                myPath = path;
            }
            else{
                cerr<<"p"<<i<<": "<<path.size()<<" ";
                pathSize[count++] = path.size();
                //oneMoreWall[count++] = checkPathForMoreWalls(path, board.players[i]);
                if(count == 1 or (count > 1 and pathSize[count-1] < enemyPath.size())){
                    enemy = i;
                    enemyPath = path;
                }
            }
        }
        float a, score = 1;
        for(int i = 0; i < count; i++){
            a = myPathSize*1.0/pathSize[i];
            score *= a;
        }        
        cerr<<"\nMe: "<<myPath.size()<<" score: "<<score<<endl;
        int criticalSize = -1;
        bool condition = false;
        bool basic = players[board.myId].wallsLeft > 0;
        bool extra = ((enemyPath.size() < 4 or enemyPath.size() < players[board.myId].wallsLeft) and  myPathSize >= enemyPath.size());
        if(basic and extra)
            condition = basic and extra;
        else{
            criticalSize = checkPathForMoreWalls(enemyPath, players[enemy]).size();
            bool critical = criticalSize < 3; 
            condition = basic and critical;
        }
        cerr<<"Condition: "<<condition<<" extra "<<extra<<" critical "<<criticalSize<<" EnemyPath: "<<enemyPath.size()<<endl;
        if(condition){
            cerr<<"Enemy is player"<<enemy<<" ";
            Wall w;
            //if(board.playerCount == 2 or deadPlayer)
                w = bfs.main(score, graph, board);
            /*else
                w = mmx.main(score, graph, board);*/
                
            cerr<<"wall: "<<w.x<<","<<w.y<<","<<w.vertical<<endl;
            
            if(w.x < 0 or w.y < 0){
                string move = "";
                move = board.getMove(players[board.myId], myPath.front());
                cout << move << endl;    
            }
            else{
                cout<<w.x<<" "<<w.y;
                if(w.vertical)
                    cout<<" V"<<endl;
                else
                    cout<<" H"<<endl;
            }
        }
        else{
            string move = "";
            move = board.getMove(players[board.myId], myPath.front());            
            cout << move << endl; 
        }
    }
}