/* 
 * File:   Player.cpp
 * Author: Medo
 *
 * Created on December 2, 2014, 9:42 AM
 */
#include<bits/stdc++.h>

using namespace std;

class Zone{
    
    int pods[4];
    public:
    int zoneId, platinum, ownerId, islandId;
    static int myId;
    
    Zone(){
        zoneId = -1;
        platinum = -1;
        ownerId = -1;
        islandId = -1;
        pods[0] = 0;
        pods[1] = 0;
        pods[2] = 0;
        pods[3] = 0;
    }
    
    void updateZone(int zId, int plat){
        zoneId = zId;
        platinum = plat;
    }
    
    void updateZone(int oId, int P0, int P1, int P2 , int P3){
        ownerId = oId;
        pods[0] = P0;
        pods[1] = P1;
        pods[2] = P2;
        pods[3] = P3;
    }
    
    int getPods(int id){
        return pods[id];
    }
    
    int getMyPods(){
        return pods[Zone::myId];
    }
    
    void addMyPods(int count){
        pods[Zone::myId] += count;
    }
    
    int getEnemyPods(){
        return pods[0]+pods[1]+pods[2]+pods[3]-pods[Zone::myId];
    }
};

int Zone::myId = 0;
Zone zones[1000];
std::vector<int> links[1000];

int head[1000] , to[1000] , nxt[1000] , lst;

int playerCount; // the amount of players (2 to 4)
int myId; // my player ID (0, 1, 2 or 3)
int zoneCount; // the amount of zones on the map
int linkCount; // the amount of links between all zones
int platinum; // my available Platinum

int attack = 20;
int defend = -1;
int neutral = 0;

int strategy = neutral;


int dist[1000];
int previous[1000];
int costs[1000][1000];

class Island{
    static int myId;
public:
    std::vector<int> totalZones;
    int totalPlatinum;
    int totalPods[4];
    int totalOwners[5];
    
    Island(){
        totalPlatinum = 0;
        for(int i = 0; i < 4; i++){
            totalPods[i] = 0;
            totalOwners[i] = 0;
        }
        totalOwners[4] = 0;
    }
    
    int size(){
        return totalZones.size();
    }
    
    void addZone(int zId){
        totalZones.push_back(zId);
        totalPlatinum += zones[zId].platinum;
    }
    
    void updateIsland(){
        myId = Zone::myId;
        for(int i = 0; i < size(); i++){
            int oId = (zones[totalZones[i]].ownerId + 5) % 5;
            totalOwners[oId]++;
            for(int j = 0; j < 4; j++)
                totalPods[j] += zones[totalZones[i]].getPods(j);
        }
    }
};

int Island::myId = 0;
std::vector<Island> islands;

void calcThreats(){
    for(int i = 0; i < linkCount; i++){
        for(int j = 0; j < int(links[i].size()); j++){
            int u = i;
            int v = links[u][j];
            if(zones[u].ownerId == myId && zones[v].ownerId == myId){
                costs[u][v] = costs[v][u] = 140;   
            }
            else if(zones[u].ownerId == myId){
                int e = strategy * min(zones[v].getEnemyPods(), 6);
                int p = 6 - zones[v].platinum;
                int l = links[v].size();
                costs[u][v] = e+p+l;   
            }
            else if(zones[v].ownerId == myId){
                int e = strategy * min(zones[u].getEnemyPods(), 6);
                int p = 6 - zones[u].platinum;
                int l = links[u].size();
                costs[v][u] = e+p+l;   
            }
            else{
                costs[u][v] = costs[v][u] = 1;   
            }
        }
    }
}

void Dijkstra(int source){
 	// Distance from source to source
	dist[source] = 0;
	std::set<std::pair<int,int> > Q;
		Q.insert(make_pair(dist[source], source));
	// Initializations
 	for(int v = 0; v < zoneCount; v++){
		if(v != source){
			// Unknown distance function from source to v
			dist[v] = INT_MAX;
			// Previous node in optimal path from source
			previous[v] = -1;
		}
		// All nodes initially in Q (unvisited nodes)
 		//Q.push_back(v);
	}
	
	// The main loop
	while(!Q.empty()){
		// Source node in first case
		//vertex in Q with min dist[u];
		int u = Q.begin()->second;
		Q.erase(Q.begin());
		
		// where v has not yet been removed from Q.
		for(int i = 0; i < int(links[u].size()); i++){
			int v = links[u][i];
			int cost = dist[u] + costs[u][v];
			// A shorter path to v has been found
			if(cost < dist[v]){
				Q.erase(make_pair(dist[v], v));
                dist[v]  = cost;
                previous[v] = u;
                Q.insert(make_pair(dist[v], v));
            }
		}
	}
}

std::list<int> getShortestPathTo(int zId)
{
    std::list<int> path;
    
    for (int v = zId; v != -1; v = previous[v]){
        path.push_front(v);
        cout<<" "<<v<<",";
    }
    return path;
}

void _init(){
    lst = 0;
    memset(head,-1,sizeof head);
}

void addLink(int a, int b){
    nxt[lst] = head[a];
    to[lst] = b;
    head[a] = lst++;
}

std::vector<int> cnt ;
int noOfnodes ;

void defineIslands(int node, int color){
    if(zones[node].islandId == color)
        return;
    zones[node].islandId = color;
    noOfnodes++;
    for(int i = head[node]; i != -1; i = nxt[i]){
        defineIslands(to[i], color);
    }
}

std::vector<vector<pair<int,int> > > islandResources;
std::vector<pair<int,int> > bestZones;
int myZonesPerIsland[10];

void getComps(){
    cnt.clear();
    cnt.push_back(0);
    
    int compN = 0 ;
    for(int i = 0 ; i < zoneCount ;i++){
        if(zones[i].islandId == -1){
            noOfnodes = 0;
            defineIslands(i, ++compN);
            cnt.push_back(noOfnodes);
        }
    }
    
    bestZones.clear();
    bestZones.resize(zoneCount);
    
    islands.clear();
    islands.resize(cnt.size());    
    
    for(int zId = 0; zId < zoneCount; zId++){
        float lp_score = zones[zId].platinum / 6.0;
        float np_score = 0;
        
        for(int j = 0; j < int(links[zId].size()); j++){
            np_score += zones[links[zId][j]].platinum / 6.0;
        }
        
        int score = (6*lp_score + np_score);
        bestZones.push_back(make_pair(score, zId));
    }
    
    sort(bestZones.rbegin(), bestZones.rend());
}

void recalculateBestScore(){
    
    islandResources.clear();
    islandResources.resize(cnt.size());
    
    for(int i = 0; i < zoneCount; i++){
        float lp_score = zones[i].platinum / 6.0;
        //float le_score = min(zones[i].getEnemyPods(), 6) / 6.0;
        float np_score = 0, ne_score = 0;
        
        for(int j = 0; j < int(links[i].size()); j++){
            np_score += zones[links[i][j]].platinum / 6.0;
            //ne_score += min(zones[links[i][j]].getEnemyPods(), 6) / 6.0;
        }
        
        //np_score = np_score / float(links[i].size());
        //ne_score = ne_score / float(links[i].size());
        
        //int score = 100*(lp_score + np_score + strategy *(ne_score + le_score));
        int score = 6*lp_score + np_score;
        islandResources[zones[i].islandId].push_back(make_pair(score, i));
    }
    
    for(int i=0; i<int(islandResources.size()); i++)
        sort(islandResources[i].rbegin(), islandResources[i].rend());
}

vector<int> getBestZone(int islandId, int count){
    vector<int> best;
    for(int i = 0; i < int(islandResources[islandId].size()); i++){
        if(int(best.size()) == count)
            break;
        if(zones[islandResources[islandId][i].second].getMyPods() > 2)
            continue;
        if(zones[islandResources[islandId][i].second].ownerId == -1 or zones[islandResources[islandId][i].second].ownerId  == myId )
            best.push_back(islandResources[islandId][i].second);
    }
    return best;
}

vector<int> getBestZone(int count){
    vector<int> best;
    for(int zId = 0; zId < zoneCount; zId++){
        int i = bestZones[zId].second;
        if(int(best.size()) == count)
            break;
        if(myZonesPerIsland[zones[i].islandId] == int(islandResources[zones[i].islandId].size()))
            continue;
        if(zones[i].ownerId == -1)
            best.push_back(i);
    }
    /*for(int zId = 0; zId < zoneCount; zId++){
        int i = bestZones[zId].second;
        if(int(best.size()) == count)
            break;
        if(zones[i].getMyPods() > 2)
            continue;
        if(myZonesPerIsland[zones[i].islandId] == int(islandResources[zones[i].islandId].size()))
            continue;
        if(zones[i].ownerId  == myId )
            best.push_back(i);
    }*/
    return best;
}

vector<int> getMoveBestZone(int islandId){
    vector<int> best;
    for(int i = 0; i < int(islandResources[islandId].size()) && int(best.size()) < 5; i++){
        int zId = islandResources[islandId][i].second;
        if(zones[zId].ownerId != myId && zones[zId].ownerId != -1)
            best.push_back(zId);
    }
    for(int i = 0; i < int(islandResources[islandId].size()) && int(best.size()) < 5; i++){
        int zId = islandResources[islandId][i].second;
        if(zones[zId].ownerId == -1)
            best.push_back(zId);
    }
    return best;
}

vector<pair<float,vector<int> > > generate(){
    
    int mypods = platinum / 20, totalPods = 0;
    float totalScore = 0;
    
    std::vector<pair<float, vector<int> > > pos;
    
    for(int i = 1; i < int(cnt.size()); i++){
        totalPods += myZonesPerIsland[i];
        vector<int> best =  getBestZone(i, mypods);
        if(best.empty())
            continue;
        if(myZonesPerIsland[i] == int(islandResources[i].size()))
            continue;
        float score = round(60*(1 - float(myZonesPerIsland[i])/(islandResources[i].size())) + zones[best[0]].platinum);
        totalScore += score;
        cerr<<i<<" "<<score<<" "<<best[0]<<endl;
        pos.push_back(make_pair(score, best));
    }
    
    if(pos.empty())
        return pos;
    
    vector<int> best = getBestZone(mypods);
    cerr<<totalPods<<endl;
    if(best.empty())
    {
        sort(pos.rbegin() , pos.rend());
        for(int i = 0; i < int(pos.size()); i++){
            int count = round(pos[i].first / totalScore * mypods);
            for(int j = 0; j < count; j++){
                cout<<1<<" "<< pos[i].second[j] <<" ";     
            }
        }
        
        /*for(int i = 0; i < mypods; i++){
            int j = i % pos.size();
            int k = i / pos.size();
            k = k % pos[j].second.size();
            cout<<1 << " "<< pos[j].second[k] <<" "; 
        }*/
        cout<<endl;
    }
    else
    {
        for(int i = 0; i < mypods; i++){
            cout<<1<<" "<< best[i]<<" "; 
        }
        cout<<endl;
    }
    return pos;
}

std::vector<int> move(){
    std::vector<int> out;
    vector<int> moveBestTargets[int(cnt.size())];
    
    int count[] = {0,0,0,0,0,0,0,0,0,0};
    
    for(int islandId = 1; islandId < int(cnt.size()); islandId++){
    	moveBestTargets[islandId] = getMoveBestZone(islandId);
    	if(moveBestTargets[islandId].empty())
    	    continue;
        
        for(int id = 0; id < int(islandResources[islandId].size()); id++){
            if(moveBestTargets[islandId].empty())
                continue;
            int index = count[islandId] / (int(islandResources[islandId].size())/2);
            index = index % moveBestTargets[islandId].size();
            if(count[islandId] % (int(islandResources[islandId].size())/2) == 0)
                Dijkstra(moveBestTargets[islandId][index]);
            int zId = islandResources[islandId][id].second;
        
            if(zones[zId].getMyPods()){
                    
                if(myZonesPerIsland[zones[zId].islandId] == islandResources[zones[zId].islandId].size() )
                    continue;
                    
                int troops = zones[zId].getMyPods();
                int flower = links[zId].size();
                
                if(troops > flower)
                    troops = troops - flower;
                else
                    troops = 1;
                    
                //cerr<<"<Island "<<islandId<<" count "<<count[islandId]<<" index "<<index<<" target "<<moveBestTargets[islandId][index]<<">"<<endl;
                    
                out.push_back(troops);
                out.push_back(zId);
                out.push_back(previous[zId]);
                zones[zId].addMyPods(-troops);
                
                count[islandId] ++;
                
                
                for(int i = 0; i < flower && zones[zId].getMyPods() > 0; i++){
                    out.push_back(1);
                    out.push_back(zId);
                    out.push_back(links[zId][i]);
                    zones[zId].addMyPods(-1);
                }
                
            }
        }
    }
    return out;
}

int main()
{
    cin >> playerCount >> myId >> zoneCount >> linkCount; cin.ignore();
    
    Zone::myId = myId;
    
    for (int i = 0; i < zoneCount; i++) {
        int zoneId; // this zone's ID (between 0 and zoneCount-1)
        int platinumSource; // the amount of Platinum this zone can provide per game turn
        cin >> zoneId >> platinumSource; cin.ignore();
        
        zones[zoneId].updateZone(zoneId, platinumSource);
    }
    
    _init();
    
    for (int i = 0; i < linkCount; i++) {
        int zone1;
        int zone2;
      
        cin >> zone1 >> zone2; cin.ignore();
        
        addLink(zone1 , zone2);
        addLink(zone2 , zone1);
        
    	links[zone1].push_back(zone2);
    	links[zone2].push_back(zone1);
    }
    
    getComps();
    
    // game loop
    
    while (1) {       
        cin >> platinum; cin.ignore();
        
        memset(myZonesPerIsland, 0, sizeof(myZonesPerIsland));
        
        for (int i = 0; i < zoneCount; i++) {
            int zId; // this zone's ID
            int ownerId; // the player who owns this zone (-1 otherwise)
            int podsCnt[4];
            cin >> zId >> ownerId >> podsCnt[0] >> podsCnt[1] >> podsCnt[2] >> podsCnt[3]; cin.ignore();
            zones[zId].updateZone(ownerId, podsCnt[0], podsCnt[1], podsCnt[2], podsCnt[3]);
            
            if(ownerId == myId){
                myZonesPerIsland[zones[zId].islandId]++;
            }
        }
        
        calcThreats();
            
        recalculateBestScore();
        
        std::vector<int> out = move();
        
        if(out.size() == 0)
            cout << "WAIT" << endl; // first line for movement commands, second line for POD purchase (see the protocol in the statement for details)
        else {
            for(int i=0; i<int(out.size()); i++)
                cout<<out[i]<<" ";
            cout<<endl;
        }
        
        if(generate().empty())
            cout<<"0 1 "<<endl;
    }
}