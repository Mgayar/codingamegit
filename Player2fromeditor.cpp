#include<bits/stdc++.h>

using namespace std;

class Zone{
    
    int pods[2];
    public:
    int zoneId, platinum, ownerId, islandId, visible, lastChecked;
    static int myId;
    
    Zone(){
        zoneId = -1;
        platinum = -1;
        ownerId = -1;
        islandId = -1;
        pods[0] = 0;
        pods[1] = 0;
        visible = 0;
		lastChecked = 0;
    }
    
    void updateZone(int zId, int plat){
        zoneId = zId;
        platinum = plat;
    }
    
    void updateZone(int oId, int P0, int P1, int v , int p){
        visible = v;
		pods[0] = P0;
		pods[1] = P1;
	    lastChecked++;
	    if(lastChecked > 5)
	        ownerId = oId;
		if(visible){
		    lastChecked = 0;
		    ownerId = oId;
			if(platinum != p){
				platinum = p;
				cerr<<zoneId<<" p: "<<p<<" oID: "<<oId<<endl;
			}
		}
    }
    
    int getPods(int id){
        return pods[id];
    }
    
    int getMyPods(){
        return pods[myId];
    }
    
    void addMyPods(int count){
        pods[myId] += count;
    }
    
    int getEnemyPods(){
        return pods[0] + pods[1] - pods[myId];
    }
    
    bool isMine(){
        return ownerId == myId;
    }
    
    bool isFree(){
        return ownerId == -1;
    }
};

int Zone::myId = 0;
Zone zones[500];
std::vector<int> links[1100];

int getMyLinkCount(int zId){

	int count = 0;
	for(int i = 0; i < int(links[zId].size()); i++)
	{
		if(zones[links[zId][i]].isMine())
			count++;
	}
	return count;
}

//int head[1000] , to[1000] , nxt[1000] , lst;

int playerCount; // the amount of players (2 to 4)
int myId; // my player ID (0, 1, 2 or 3)
int zoneCount; // the amount of zones on the map
int linkCount; // the amount of links between all zones
int platinum; // my available Platinum

int attack = 20;
int defend = -1;
int neutral = 1;

int strategy = neutral;

int myBase=-1, enemyBase=-1;//, thePainter = -1, target = -1, bestPainter = -1, bestTarget = -1;

int dist[1000];
int previous[1000];
int costs[1000][1000];

class Island{
    static int myId;
public:
    std::vector<int> totalZones;
    int totalPlatinum;
    int totalPods[2];
    int totalOwners[3];
	int bestPainter, bestTarget;
	int painter, target;
    
    Island(){
		bestPainter = painter = bestTarget = target = -1;
        totalPlatinum = 0;        
        memset(totalOwners, 0, sizeof(totalOwners));
        memset(totalPods, 0, sizeof(totalPods));
    }
    
    int size(){
        return totalZones.size();
    }
    
    void addZone(int zId){
        totalZones.push_back(zId);
        totalPlatinum += zones[zId].platinum;
    }
	
	int getEnemyPlatinum(){
		return totalOwners[0] + totalOwners[1] - totalOwners[myId];
	}
	
	int getMyPlatinum(){
		return totalOwners[myId];
	}
	
	int getEnemyPods(){
		return totalPods[0] + totalPods[1] - totalPods[myId];
	}
	
	int getMyPods(){
		return totalPods[myId];
	}
    
    void updateIsland(){
        myId = Zone::myId;
        memset(totalOwners, 0, sizeof(totalOwners));
        memset(totalPods, 0, sizeof(totalPods));
//		cerr<<"TO: "<<totalOwners[2];
        for(int i = 0; i < size(); i++){
            int zId = totalZones[i];
            int oId = (zones[zId].ownerId + (playerCount + 1)) % (playerCount + 1);
            totalOwners[oId] += zones[zId].platinum;
            for(int j = 0; j < playerCount; j++)
                totalPods[j] += zones[zId].getPods(j);
        }
    }
};

int Island::myId = 0;
std::vector<Island> islands;

void calcThreats(){
    for(int i = 0; i < linkCount; i++){
        for(int j = 0; j < int(links[i].size()); j++){
            int u = i;
            int v = links[u][j];
            if(zones[u].isMine() && zones[v].isMine()){
                costs[u][v] = costs[v][u] = 6*25;
            }
            else if(zones[u].isMine()){
                int e = 6 - strategy * min(zones[v].getEnemyPods(), 6);
                int p = 6 - zones[v].platinum;
                int l = strategy * links[v].size();
                costs[u][v] = e+p+l;   
            }
            else if(zones[v].isMine()){
                int e = 6 - strategy * min(zones[u].getEnemyPods(), 6);
                int p = 6 - zones[u].platinum;
                int l = strategy * links[u].size();
                costs[v][u] = e+p+l;
            }
            else{
                int p = 6 - zones[u].platinum;
                p = min(p, 6 - zones[v].platinum);
                costs[u][v] = costs[v][u] = p;   
            }
                int p = 7 - zones[u].platinum;
                p = min(p, 7 - zones[v].platinum);
                //costs[u][v] = costs[v][u] = 1; 
        }
    }
}

void Dijkstra(int source){
 	// Distance from source to source
	dist[source] = 0;
	std::set<std::pair<int,int> > Q;
		Q.insert(make_pair(dist[source], source));
	// Initializations
 	for(int v = 0; v < zoneCount; v++){
		if(v != source){
			// Unknown distance function from source to v
			dist[v] = INT_MAX;
			// Previous node in optimal path from source
			previous[v] = -1;
		}
		// All nodes initially in Q (unvisited nodes)
 		//Q.push_back(v);
	}
	
	// The main loop
	while(!Q.empty()){
		// Source node in first case
		//vertex in Q with min dist[u];
		int u = Q.begin()->second;
		Q.erase(Q.begin());
		
		// where v has not yet been removed from Q.
		for(int i = 0; i < int(links[u].size()); i++){
			int v = links[u][i];
			int cost = dist[u] + costs[u][v];
			// A shorter path to v has been found
			if(cost < dist[v]){
				Q.erase(make_pair(dist[v], v));
                dist[v]  = cost;
                previous[v] = u;
                Q.insert(make_pair(dist[v], v));
            }
		}
	}
}

int bfsTargetByPainter(int zId, int dontUseThisZone){
    bool visited[500];
    int backup = -1;
    std::list<int> Q;
    Q.push_back(zId);
	
    memset(visited, 0, sizeof(visited));
	// The main loop
    while(!Q.empty()){
        int u = Q.front();
        Q.erase(Q.begin());
        
        // where v has not yet been removed from Q.
        for(int i = 0; i < int(links[u].size()); i++){
            int v = links[u][i];
            if(visited[v])
                continue;//&& zones[v].getEnemyPods() == 0
            visited[v] = true;
            if(!zones[v].isMine() && zones[v].platinum >= 0 && v != dontUseThisZone)
                return v;
            if(!zones[v].isMine() && !zones[v].visible && v != dontUseThisZone && backup == -1)
                backup = v;
            Q.push_back(v);
        }
    }
    return backup;
}

int bfsPainterByTarget(int zId, int dontUseThisZone){
    bool visited[500];
    std::list<int> Q;
    Q.push_back(zId);
	
    memset(visited, 0, sizeof(visited));
	// The main loop
    while(!Q.empty()){
        int u = Q.front();
        Q.erase(Q.begin());
        // where v has not yet been removed from Q.
        for(int i = 0; i < int(links[u].size()); i++){
            int v = links[u][i];
            if(visited[v])
                continue;
            visited[v] = true;
            if(zones[v].isMine() && zones[v].getMyPods() > 0 && v != dontUseThisZone){
                //cerr<<dontUseThisZone<<endl;
                return v;
            }
            Q.push_back(v);
        }
    }
    return -1;
}

std::list<int> getShortestPathTo(int zId)
{
    std::list<int> path;
    
    for (int v = zId; v != -1; v = previous[v]){
        path.push_front(v);
        cout<<" "<<v<<",";
    }
    return path;
}

std::vector<int> cnt ;

int defineIslands(int zId, int color){
    std::list<int> Q;
    Q.push_back(zId);
    int count = 0;
    // The main loop
    while(!Q.empty()){
        int u = Q.front();
        Q.erase(Q.begin());
        zones[u].islandId = color;
        count++;
        // where v has not yet been removed from Q.
        for(int i = 0; i < int(links[u].size()); i++){
            int v = links[u][i];
            if(zones[v].islandId == color)
                continue;
            Q.push_back(v);
        }
    }
    return count;
}

std::vector<vector<pair<int,int> > > islandResources;
std::vector<pair<int,int> > bestZones;
int myZonesPerIsland[10];

void getComps(){
    cnt.clear();
    cnt.push_back(0);
    
    cerr<<"1();";
    int color = 0 ;
	
    cerr<<zoneCount<<" "<<linkCount<<";";
    //391 832
    //431 1039;
    if(linkCount < 220){
        for(int i = 0 ; i < zoneCount ;i++)
            if(zones[i].islandId == -1){
                int count = defineIslands(i, ++color);
                cnt.push_back(count);
            }
    }
    else{
        for(int i = 0 ; i < zoneCount ;i++)
            if(zones[i].islandId == -1){
                zones[i].islandId = 1;
            }
        cnt.push_back(zoneCount);
    }
        
    //cnt.push_back(zoneCount);
    
    cerr<<"2();";
    
    bestZones.clear();
    bestZones.resize(zoneCount);
    
    islands.clear();
    islands.resize(cnt.size());    
    
    for(int zId = 0; zId < zoneCount; zId++){
        float lp_score = zones[zId].platinum / 6.0;
        float np_score = 0;
        
        for(int j = 0; j < int(links[zId].size()); j++){
            np_score += zones[links[zId][j]].platinum / 6.0;
        }
        
        int score = (6*lp_score + np_score);
        islands[zones[zId].islandId].addZone(zId);
        bestZones.push_back(make_pair(score, zId));
    }
    
    cerr<<"3();";
    sort(bestZones.rbegin(), bestZones.rend());
}

void recalculateBestScore(){
    
    islandResources.clear();
    islandResources.resize(cnt.size());
    
    for(int i = 0; i < zoneCount; i++){
        float lp_score = zones[i].platinum / 6.0;
        //float le_score = min(zones[i].getEnemyPods(), 6) / 6.0;
        float np_score = 0, ne_score = 0;
        
        for(int j = 0; j < int(links[i].size()); j++){
            np_score += zones[links[i][j]].platinum / 6.0;
            //ne_score += min(zones[links[i][j]].getEnemyPods(), 6) / 6.0;
        }
        
        //np_score = np_score / float(links[i].size());
        //ne_score = ne_score / float(links[i].size());
        
        //int score = 100*(lp_score + np_score + strategy *(ne_score + le_score));
        int score = 6*lp_score + np_score;
        islandResources[zones[i].islandId].push_back(make_pair(score, i));
    }
    
    for(int i=0; i<int(islandResources.size()); i++){
        islands[i].updateIsland();
        sort(islandResources[i].rbegin(), islandResources[i].rend());
    }
}

vector<int> getBestZone(int islandId, int count){
    vector<int> best;
    for(int i = 0; i < int(islandResources[islandId].size()); i++){
        if(int(best.size()) == count)
            break;
        if(zones[islandResources[islandId][i].second].getMyPods() > 2)
            continue;
        if(zones[islandResources[islandId][i].second].isMine() )
            best.push_back(islandResources[islandId][i].second);
    }
    return best;
}

vector<int> getBestZone(int count){
    vector<int> best;
  /*  for(int zId = 0; zId < zoneCount; zId++){
        int i = bestZones[zId].second;
        if(int(best.size()) == count)
            break;
        if(myZonesPerIsland[zones[i].islandId] == int(islandResources[zones[i].islandId].size()))
            continue;
        if(zones[i].ownerId == -1)
            best.push_back(i);
    }*/
    for(int zId = 0; zId < zoneCount; zId++){
        int i = bestZones[zId].second;
        if(int(best.size()) == count)
            break;
        if(zones[i].getMyPods() > 2)
            continue;
        if(myZonesPerIsland[zones[i].islandId] == int(islandResources[zones[i].islandId].size()))
            continue;
        if(zones[i].isMine() )
            best.push_back(i);
    }
    return best;
}

vector<int> getMoveBestZone(int islandId){
    vector<int> best;
    best.push_back(enemyBase);
    for(int i = 0; i < int(islandResources[islandId].size()) && int(best.size()) < 2; i++){
        int zId = islandResources[islandId][i].second;
        if(!zones[zId].isMine())
            best.push_back(zId);
    }
    for(int i = 0; i < int(islandResources[islandId].size()) && int(best.size()) < 2; i++){
        int zId = islandResources[islandId][i].second;
        if(zones[zId].isFree())
            best.push_back(zId);
    }
    return best;
}

vector<pair<float,vector<int> > > generate(){
    
    int mypods = platinum / 20, totalPods = 0;
    float totalScore = 0;
    
    std::vector<pair<float, vector<int> > > pos;
    
    for(int i = 1; i < int(cnt.size()); i++){
        totalPods += myZonesPerIsland[i];
        vector<int> best =  getBestZone(i, mypods);
        if(best.empty())
            continue;
        if(myZonesPerIsland[i] == int(islandResources[i].size()))
            continue;
        float score = round(60*(1 - float(myZonesPerIsland[i])/(islandResources[i].size())) + zones[best[0]].platinum);
        totalScore += score;
        cerr<<i<<" "<<score<<" "<<best[0]<<endl;
        pos.push_back(make_pair(score, best));
    }
    
    if(pos.empty())
        return pos;
    
    vector<int> best = getBestZone(mypods);
    cerr<<totalPods<<endl;
    if(best.empty())
    {
        sort(pos.rbegin() , pos.rend());
        for(int i = 0; i < int(pos.size()); i++){
            int count = round(pos[i].first / totalScore * mypods);
            for(int j = 0; j < count; j++){
                cout<<1<<" "<< pos[i].second[j] <<" ";     
            }
        }
        cout<<endl;
    }
    else
    {
        for(int i = 0; i < mypods; i++){
            if(links[best[i]].size() > 3)
                cout<<1<<" ";
            else
                cout<<1<<" ";
            cout<< best[i]<<" ";
        }
        cout<<endl;
    }
    return pos;
}

std::vector<int> move(){
    std::vector<int> out;
    vector<int> moveBestTargets[int(cnt.size())];
    
    int count[] = {0,0,0,0,0,0,0,0,0,0};
    
    for(int islandId = 1; islandId < int(cnt.size()); islandId++){
    	moveBestTargets[islandId] = getMoveBestZone(islandId);
    	
        cerr<<islands[islandId].getMyPods()<<" <> "<<(islands[islandId]).getEnemyPods()<<" | ";
        cerr<<islands[islandId].getMyPlatinum()<<" <"<<islands[islandId].totalOwners[playerCount]
            <<"> "<<islands[islandId].getEnemyPlatinum()<<endl;
            
    	if(moveBestTargets[islandId].empty())
    	    continue;
        
		int painter = -1;
        
		if(islands[islandId].painter != -1){
			int troops = zones[islands[islandId].painter].getMyPods();
			if(troops !=0){
				if(moveBestTargets[islandId].size() > 0){
					if(islands[islandId].target == -1 || zones[islands[islandId].target].isMine()){
						//if(islands[islandId].getMyPlatinum() < (islands[islandId].getEnemyPlatinum() + islands[islandId].totalOwners[playerCount]))
							islands[islandId].target = bfsTargetByPainter(islands[islandId].painter, islands[islandId].bestTarget);
						//else
							//islands[islandId].target = bfsTargetByPainter(myBase, islands[islandId].bestTarget);
					}
					Dijkstra(islands[islandId].target);
				}
				
				cerr<<"painter: ("<<troops<<") "<<islands[islandId].painter<<"->"<<islands[islandId].target<<endl;
				out.push_back(1);
				out.push_back(islands[islandId].painter);
				out.push_back(previous[islands[islandId].painter]);
				zones[islands[islandId].painter].addMyPods(-1);
				
				painter = islands[islandId].painter;
				
				islands[islandId].painter = previous[islands[islandId].painter];
			}
			else{
				islands[islandId].painter = -1;
				islands[islandId].target = -1;
			}
		}
		
		if(islands[islandId].bestTarget == -1 or zones[islands[islandId].bestTarget].isMine()){
            if(moveBestTargets[1].size() > 1){
                islands[islandId].bestTarget = moveBestTargets[islandId][1];
                islands[islandId].bestTarget = bfsTargetByPainter(myBase, islands[islandId].target);
            }
        }
        islands[islandId].bestPainter = bfsPainterByTarget(islands[islandId].bestTarget, painter);

        if(islands[islandId].bestPainter != -1){
            Dijkstra(islands[islandId].bestTarget);
            int troops = zones[islands[islandId].bestPainter].getMyPods();
            if(troops !=0){
                cerr<<"best painter: ("<<troops<<") "<<islands[islandId].bestPainter<<"->"<<islands[islandId].bestTarget<<endl;
                out.push_back(1);
                out.push_back(islands[islandId].bestPainter);
                out.push_back(previous[islands[islandId].bestPainter]);
                zones[islands[islandId].bestPainter].addMyPods(-1);
                islands[islandId].bestPainter = previous[islands[islandId].bestPainter];
            }
            else{
                islands[islandId].bestPainter = -1;
            }
        }
        
        for(int id = 0; id < int(islandResources[islandId].size()); id++){
            if(moveBestTargets[islandId].empty())
                continue;
            int index = count[islandId] / (int(islandResources[islandId].size())/1);
            index = index % moveBestTargets[islandId].size();
            if(count[islandId] % (int(islandResources[islandId].size())/1) == 0)
                Dijkstra(moveBestTargets[islandId][index]);
            int zId = islandResources[islandId][id].second;
            
            //if(zId == islands[islandId].painter)
                //continue;
            
            if(zones[zId].getMyPods()){
                    
                if(myZonesPerIsland[zones[zId].islandId] == islandResources[zones[zId].islandId].size() )
                    continue;
                    
                int troops = zones[zId].getMyPods();
                int flower = links[zId].size() - getMyLinkCount(zId);
                
                int maxp = zones[previous[zId]].platinum;
                int target = previous[zId];
                
                for(int i = 0; i < flower && zones[zId].getMyPods() > 0; i++){
                    if(zones[links[zId][i]].platinum > maxp && !zones[links[zId][i]].isMine()){
                        target = links[zId][i];
                        maxp = zones[target].platinum;
                    }
                    if(zones[links[zId][i]].isMine())
                        continue;
                    if(troops > flower){
                        out.push_back(1);
                        out.push_back(zId);
                        out.push_back(links[zId][i]);
                        zones[zId].addMyPods(-1);
                        flower--;
                    }
                }
                
                troops = zones[zId].getMyPods();
                if(islands[islandId].painter == -1 && troops > 0){
                    islands[islandId].painter = zId;
                    troops--;
                    //continue;
                }
                
                out.push_back(troops);
                out.push_back(zId);
                //out.push_back(previous[zId]);
                out.push_back(target);
                zones[zId].addMyPods(-troops);
                
                count[islandId] ++;
                
            }
        }
    }
    return out;
}

int main()
{
    cin >> playerCount >> myId >> zoneCount >> linkCount; cin.ignore();
    
    Zone::myId = myId;
    
    for (int i = 0; i < zoneCount; i++) {
        int zoneId; // this zone's ID (between 0 and zoneCount-1)
        int platinumSource; // the amount of Platinum this zone can provide per game turn
        cin >> zoneId >> platinumSource; cin.ignore();
        
        zones[zoneId].updateZone(zoneId, platinumSource);
    }
    
//    _init();
    
    for (int i = 0; i < linkCount; i++) {
        int zone1;
        int zone2;
      
        cin >> zone1 >> zone2; cin.ignore();
        
//        addLink(zone1 , zone2);
//        addLink(zone2 , zone1);
        
    	links[zone1].push_back(zone2);
    	links[zone2].push_back(zone1);
    }
    
    cerr<<"getComps();";
    getComps();
    cerr<<"loop();";
    
    // game loop
    
    while (1) {       
        cin >> platinum; cin.ignore();
        
        memset(myZonesPerIsland, 0, sizeof(myZonesPerIsland));
        
        for (int i = 0; i < zoneCount; i++) {
            int zId; // this zone's ID
            int ownerId; // the player who owns this zone (-1 otherwise)
            int podsCnt[2], v, p;
            cin >> zId >> ownerId >> podsCnt[0] >> podsCnt[1] >> v >> p; cin.ignore();
            zones[zId].updateZone(ownerId, podsCnt[0], podsCnt[1], v, p);
            
            if(ownerId == myId){
                myZonesPerIsland[zones[zId].islandId]++;
                if(myBase == -1)
                    myBase = zId;
            }else if(ownerId != -1 && enemyBase == -1){
                enemyBase = zId;
            }
        }
        cerr<<"calcThreats();";
        calcThreats();
        cerr<<"recalculateBestScore();";    
        recalculateBestScore();
        
        cerr<<"move();"<<endl;
        std::vector<int> out = move();
        
        
        if(out.size() == 0)
            cout << "WAIT" << endl; 
        else {
            for(int i=0; i<int(out.size()); i++)
                cout<<out[i]<<" ";
            cout<<endl;
        }
        cout << "WAIT" << endl; 
        /*cerr<<"generate();";
        if(generate().empty())
            cout<<"0 1 "<<endl;*/
    }
}